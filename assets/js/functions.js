function postalcode() {

    v = $('#postal_code').val();
    v = v.replace(/\D/g, "");
    v = v.replace(/^(\d{4})(\d)/g, "$1-$2");

    $('#postal_code').val(v);
}

function onlynumbers() {
    v = $('#nif').val();
    var res = v.match(/\D/g);

    if (res !== null) {
        $('#nif').addClass("input-danger");

    } else {
        $('#nif').removeClass("input-danger");

    }
    v = v.replace(/\D/g, "");
    $('#nif').val(v);
}

function phonePortugal() {
    conytry = $('#contry_id').val();
    v = $('#phone').val();
    v = v.replace(/\D/g, "");
    if (conytry == '351') {
        v = v.replace(/(\d{3})(\d)/, "$1 $2");
        v = v.replace(/(\d{3})(\d)/, "$1 $2");
        v = v.replace(/(\d{3})(\d{3})$/, "$1 $2");
    }
    $('#phone').val(v);

}

function testPassword() {
    pwString = $('#password').val();
    var strength = 0;

    strength += /[A-Z]+/.test(pwString) ? 1 : 0;
    strength += /[a-z]+/.test(pwString) ? 1 : 0;
    strength += /[0-9]+/.test(pwString) ? 1 : 0;
    strength += /[\W]+/.test(pwString) ? 1 : 0;
    //strength += /\d{6}?/.test(pwString) ? 1 : -1;

    switch (strength) {
        case 3:
            $(".itempassword").css("float", "");
            $(".itempassword").css("marginLeft", "35px");

            break;
        case 4:
            $(".itempassword").css("marginLeft", "");
            $(".itempassword").css("float", "right");

            break;
        default:
            $(".itempassword").css("marginLeft", "");
            $(".itempassword").css("float", "left");

            break;
    }
}

function preenche() {
    postal_code = $('#postal_code').val();
    $.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + postal_code + '&key=AIzaSyCNwS-HDfNf66EdZOLrTZxuTCpD7JJ-7ig', function (data) {


        if (data.status == 'OK') {
            var obj = data.results[0].address_components
            $('#location').val(obj[2].long_name);
        }

    });
}

function valida_email_exist() {
    email = $('#email').val();
    $.get('valida_email.php?email=' + email, function (data) {
        var result = JSON.parse(data);
        if (result.status > 0) {
            $('#email').addClass("input-danger");
            $('#emailCad').show();
            return false;

        } else {
            $('#email').removeClass("input-danger");
            $('#emailCad').hide();
            return true;
        }

    });
    
}

function valida_email_two() {
    if ($('#email_two').length > 0 && ($('#email').val() !== $('#email_two').val())) {
        $('#email_two').addClass("input-danger");
        $('#emailRep').show();
        return false;

    } else {
        $('#email_two').removeClass("input-danger");
        $('#emailRep').hide();
        return true;
    }
    return true;
}

function valida_password_two() {

    if ($('#password_two').val() != "" && ($('#password_two').val() !== $('#password').val())) {
        $('#password_two').addClass("input-danger");
        $('#passwordWrong').show();
        return false;

    } else {
        $('#password_two').removeClass("input-danger");
        $('#passwordWrong').hide();
        return true;
    }
    return true;
}

function valida_email() {
    v = $('#email').val();
    var res = v.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

    if (res === null) {
        $('#email').addClass("input-danger");
        return false;
    } else {
        $('#email').removeClass("input-danger");
        return true;
    }
    return true;
}

function valida_campos() {
    if (valida_email() && valida_password_two() && valida_email_two() && $('#emailCad').css('display') === 'none' ) {
        return true;
    } else {

        return false;
    }

}
