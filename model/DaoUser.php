<?php

include 'config/Database.php';

class DaoUser {

    public static $instance;

    private function __construct() {
        //
    }

    public static function getInstance() {
        if (!isset(self::$instance))
            self::$instance = new DaoUser();

        return self::$instance;
    }

    private $name;
    private $alias;
    private $email;
    private $password;
    private $street;
    private $postal_code;
    private $location;
    private $contry;
    private $nif;
    private $phone;

    public function Insert(User $usuario) {
        try {
            $sql = "INSERT INTO user (    
                  name,
                  alias,
                  email,
                  password,
                  street,
                  postal_code,
                  location,
                  contry_id,
                  nif,
                  phone)
                  VALUES (
                  :name,
                  :alias,
                  :email,
                  :password,
                  :street,
                  :postal_code,
                  :location,
                  :contry,
                  :nif,
                  :phone)";

            $p_sql = Database::getInstance()->prepare($sql);

            $p_sql->bindValue(":name", $usuario->getName());
            $p_sql->bindValue(":alias", $usuario->getAlias());
            $p_sql->bindValue(":email", $usuario->getEmail());
            $p_sql->bindValue(":password", $usuario->getPassword());
            $p_sql->bindValue(":street", $usuario->getStreet());
            $p_sql->bindValue(":postal_code", $usuario->getPostal_code());
            $p_sql->bindValue(":location", $usuario->getLocation());
            $p_sql->bindValue(":contry", $usuario->getContry());
            $p_sql->bindValue(":nif", $usuario->getNif());
            $p_sql->bindValue(":phone", $usuario->getPhone());


            return $p_sql->execute();
        } catch (Exception $e) {
            echo $e->getMessage();
             print "Ocorreu um erro ao tentar executar esta ação, foi gerado um LOG do mesmo, tente novamente mais tarde.";
        }
    }

    public function SearchByEmail($email) {
        try {
            $sql = "SELECT * FROM user where email = '$email' ";
            $result = Database::getInstance()->query($sql);
            $lista = $result->fetchAll(PDO::FETCH_ASSOC);
            $f_lista = array();

            foreach ($lista as $l)
                $f_lista[] = $l;

            return $f_lista;
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, foi gerado um LOG do mesmo, tente novamente mais tarde.";
        }
    }

    public function ListAll() {
        try {
            $sql = "SELECT * FROM user order by id";
            $result = Database::getInstance()->query($sql);
            $lista = $result->fetchAll(PDO::FETCH_ASSOC);
            $f_lista = array();

            foreach ($lista as $l)
                $f_lista[] = $l;

            return $f_lista;
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, foi gerado um LOG do mesmo, tente novamente mais tarde.";
        }
    }

}

?>
