<?php
   
  class User {
   
      private $id;
      private $name;
      private $alias;
      private $email;
      private $password;
      private $street;
      private $postal_code;
      private $location;
      private $contry;
      private $nif;
      private $phone;
      
      
      function getEmail() {
          return $this->email;
      }

      function getPassword() {
          return $this->password;
      }

      function getName() {
          return $this->name;
      }

      function getAlias() {
          return $this->alias;
      }

      function getStreet() {
          return $this->street;
      }

      function getPostal_code() {
          return $this->postal_code;
      }

      function getLocation() {
          return $this->location;
      }

      function getContry() {
          return $this->contry;
      }

      function getNif() {
          return $this->nif;
      }

      function getPhone() {
          return $this->phone;
      }

      function setEmail($email) {
          $this->email = $email;
      }

      function setPassword($password) {
          $this->password = $password;
      }

      function setName($name) {
          $this->name = $name;
      }

      function setAlias($alias) {
          $this->alias = $alias;
      }

      function setStreet($street) {
          $this->street = $street;
      }

      function setPostal_code($postal_code) {
          $this->postal_code = $postal_code;
      }

      function setLocation($location) {
          $this->location = $location;
      }

      function setContry($contry) {
          $this->contry = $contry;
      }

      function setNif($nif) {
          $this->nif = $nif;
      }

      function setPhone($phone) {
          $this->phone = $phone;
      }


   
  }
   
  ?>