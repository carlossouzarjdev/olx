<!doctype html>
<html class="no-js" lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="assets/css/normalize.min.css">
        <link rel="stylesheet" href="assets/css/main.css">

    </head>

    <body>
        <main>
            <section class="container">
                <div class="content-form">
                    <form action="config.php" method="post" id="form" onsubmit="return valida_campos();">

                        <h1 class="titles">Configure o seu Banco de Dados</h1>
                        <h2 class="titles">Vamos definir o host, database, usuario e senha</h2>
                        <div class="step">
                            <div class="form-group">
                                <label for="email" class="col-4">Host <span class="danger">*</span></label>
                                <input type="text" maxlength="35" name="host" value="localhost" class="form-input col-8" id="host" required x-moz-errormessage="HOST obrigatório" title="HOST obrigatório" >
                            </div>
                            <div class="form-group">
                                <label for="email_two" class="col-4">Database <span class="danger">*</span></label>
                                <input type="text" maxlength="35" class="form-input col-8" id="database"  name="database" required title="DATABASE é obrigatório" x-moz-errormessage="DATABASE é obrigatório" placeholder="nome de sua database">
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-4">User <span class="danger">*</span></label>
                                <input type="text"  maxlength="38" name="user" class="form-input col-8" id="user" required x-moz-errormessage="Usuario é obrigatório" title="Usuario é obrigatório" placeholder="Usuario do banco de dados">
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-4">Senha </label>
                                <input type="password"  maxlength="38" name="password" class="form-input col-8" id="password" placeholder="Senha do BD" >
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-4">Valida ? </label>
                                <button type="button" class="btn col-4" onclick="validaConexao()">Validar conexao</button>
                                <span id="message" class="col-8"></span>
                            </div>
                            <button type="button" class="btn" style="display: none" onclick="CriaBD()" id="btnBD">Criar Tabelas</button>
                    </form>
                </div>
            </section>
        </main>

        <script>
            function validaConexao() {
                host = $('#host').val();
                database = $('#database').val();
                user = $('#user').val();
                password = $('#password').val();

                $.post("checkBD.php", {host: host, database: database, user: user, password: password, type: 'valid'}, function (data) {
                    if (data.status == '1') {
                        $("#message").html("-> TUDO OK! ");
                        $("#btnBD").show();
                    } else {
                        $("#message").html("-> Erro: " + data.message);
                        $("#btnBD").hide();
                    }

                }, "json");
            }

            function CriaBD() {
                host = $('#host').val();
                database = $('#database').val();
                user = $('#user').val();
                password = $('#password').val();

                $.post("checkBD.php", {host: host, database: database, user: user, password: password, type: 'create'}, function (data) {
                    if (data.status == '1') {
                        window.location.assign("index.php")
                    } else {
                        $("#message").html("-> Erro: " + data.message);
                        $("#btnBD").hide();
                    }

                }, "json");
            }
        </script>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    </body>

</html>