<!doctype html>
<html class="no-js" lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="assets/css/normalize.min.css">
        <link rel="stylesheet" href="assets/css/main.css">

        <script src="assets/js/functions.js"></script>
        <script>


            function statusChangeCallback(response) {
                if (response.status === 'connected') {
                    $("#fb").hide();
                    Preencher();
                }
            }

            function checkLoginState() {
                FB.getLoginStatus(function (response) {
                    statusChangeCallback(response);
                });
            }

            window.fbAsyncInit = function () {
                FB.init({
                    appId: '187706075048280',
                    cookie: true, // enable cookies to allow the server to access 
                    // the session
                    xfbml: true, // parse social plugins on this page
                    version: 'v2.8' // use graph api version 2.8
                });

                FB.getLoginStatus(function (response) {
                    statusChangeCallback(response);
                });

            };
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            function Preencher() {
                FB.api('/me?fields=name,email', function (response) {
                    $('#email').val(response.email);
                    $('#email_two').val(response.email);
                    $('#name').val(response.name);
                    valida_email_exist();

                });
            }


        </script>
    </head>

    <body>
        <main>
            <section class="container">
                <div class="content-form">
                    <form action="insert.php" method="post" id="form" onsubmit="return valida_campos();">
                        <?php
                        if (isset($erros) && is_array($erros)) {
                            foreach ($erros as $erro) {
                                ?>
                                <span class="col-4 danger"> *<?= $erro ?> </span>

                                <?php
                            }
                        }
                        ?>
                        <h1 class="titles">Registre-se gratuitamente</h1>
                        <h2 class="titles">Resgistre-se de forma fácil e rápida. O registro é rápido e grátis</h2>
                        <div class="fb-login-button" scope="public_profile,email" onlogin="checkLoginState();" id="fb" data-max-rows="1" data-size="medium" data-show-faces="false" data-auto-logout-link="false"></div>
                        <div class="step">
                            <div class="form-group">
                                <label for="email" class="col-4">Email <span class="danger">*</span></label>
                                <input type="email" maxlength="35" onkeyup="valida_email();valida_email_exist();valida_email_two();" name="email" class="form-input col-8" id="email" required x-moz-errormessage="Email obrigatório" title="Email obrigatório" placeholder="Email">
                                <span class="col-6 danger" style="display: none" id="emailCad"> -> Email já cadastrado! </span>
                            </div>
                            <div class="form-group">
                                <label for="email_two" class="col-4">Confirmar Email <span class="danger">*</span></label>
                                <input type="email" maxlength="35" class="form-input col-8" id="email_two" onkeyup="valida_email_two();" name="email2" required title="Confirmar email é obrigatório" x-moz-errormessage="Confirmar email é obrigatório" placeholder="Confirmar Email">
                                <span class="col-6 danger" style="display: none" id="emailRep"> -> Confirme o email acima! </span>
                            </div>
                        </div>
                        <div class="step">
                            <div class="form-group">
                                <label for="password" class="col-4">Password <span class="danger">*</span></label>
                                <input type="password"  maxlength="38" name="password" class="form-input col-6" id="password" onkeyup="testPassword();valida_password_two();" required x-moz-errormessage="Senha é obrigatório" title="Senha é obrigatório" placeholder="Senha">
                                <div id="grad" class="col-6"><div class="itempassword" id="passwordStreng"></div></div><br>

                            </div>
                            <div class="form-group">
                                <label for="password_two" class="col-4">Confirmar password <span class="danger">*</span></label>
                                <input type="password" maxlength="38" name="password2" class="form-input col-5"  onkeyup="valida_password_two();" id="password_two" required title="Confirmar senha é obrigatório" x-moz-errormessage="Confirmar senha é obrigatório" placeholder="Confirmar Senha">
                                <span class="col-6 danger" style="display: none" id="passwordWrong"> -> As senhas são diferentes! </span>
                            </div>
                        </div>
                        <div class="step">
                            <div class="form-group">
                                <label for="name" class="col-4">Nome <span class="danger">*</span></label>
                                <div class="form-group">
                                    <input type="text" maxlength="20" name="name" class="form-input col-6" id="name" required placeholder="Nome" title="Nome é obrigatório" x-moz-errormessage="Nome é obrigatório">
                                    <input type="text" maxlength="18" name="alias" class="form-input col-6 align" id="nickname" placeholder="Apelido" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="zip_code" class="col-4">Código postal / Localidade</label>
                                <div class="form-group">
                                    <input type="text" name="postal_code" maxlength="8" class="form-input col-4" id="postal_code" onkeyup="postalcode()" onblur="preenche()" >
                                    <input type="text" maxlength="50"name="location" id="location" class="form-input col-8 align" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address" class="col-4">Rua / N&deg;</label>
                                <input type="text" maxlength="40" name="street" class="form-input col-8" id="address">
                            </div>
                            <div class="form-group">
                                <label for="nation" class="col-4">País</label>
                                <div class="col-6">
                                    <select name="contry_id" id="contry_id" class="form-input">
                                        <option value="54">Argentina</option>
                                        <option value="55">Brasil</option>
                                        <option value="34">Espanha</option>
                                        <option value="351">Portugal</option>
                                    </select>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="nif" class="col-4">NIF</label>
                                <input type="text" name="nif" onkeyup="onlynumbers()" maxlength="9" class="form-input col-6" id="nif" >
                            </div>
                            <div class="form-group">
                                <label for="phone" class="col-4">Telefone</label>
                                <input type="tel" name="phone"  maxlength="15" onkeyup="phonePortugal()" class="form-input col-6" id="phone">
                            </div>
                        </div>
                        <button type="submit" class="btn">Registro</button>
                    </form>
                </div>
            </section>
        </main>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    </body>

</html>
