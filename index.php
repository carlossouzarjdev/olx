<?php

include 'config/Template.php';

$filename = 'config/Const.php';

if (file_exists($filename)) 
    $tmpl = new Template('view/form.php');
 else 
    $tmpl = new Template('view/config.php');

echo $tmpl->render();
