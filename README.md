# LEIAME #

Este projeto é uma proposta ao desafio lançado pela OLX.

Peguei este desafio na quarta a noite, e entreguei na quinta a noite, como trabalho durante o dia, pelos meu cálculos, o projeto todo levou umas ~~6 a 7 horas para ser feito~~ (atualizo para umas 10 horas, colocando o wizard para montar as estruturas de tabelas dinâmicas e uma documentação boa de ser lida ;) =D 
(*logo, pode ser que tenha algo a ser ajustado ou refatorado....se tiver, por favor, contribua*)

Projeto rodando [aqui](http://carlossouza.com/olx/)

# O GRANDE DESAFIO #

![Alt text](https://at-cdn-s01.audiotool.com/2014/02/26/documents/fixit_-_challenge_accepted/2/cover256x256-adb5b94f4a6a4017a4933d9c63e696b3.jpg)

O maior desafio **pra mim** foi ter que desenvolver todo o projeto sem nenhum auxílio de Frameworks....

Admito que já estava me preparando para usar meu bom e velho CodeIgniter3 + Bootstrap + PhPUnit..... mas acho q consegui fazer um projeto interessante.

Tentei utilizar alguns padrões de projeto, segue alguns exemplos:

*Singleton:*
*Tentei utilizar na classe Database, o que nos garante apenas uma instância na aplicação. 

*MVC:*
*~~Admito que aqui senti muita falta de um framework~~, mas criei uma config que consegue carregar as views separadas para o projeto, e ainda podendo passar dados para o template
, consegui separar a camada de negócio de template, e a controller que devo refatorar.

*Onde tive mais dificuldade foi a implementação de testes sem o *PhPUnit* (fazer testes sem framework foi o meu maior desafio) , mas consegui gerar um entregável desta proposta (mas acredito que consigo melhorar)

# SEGURANÇA #

Um dos pontos do desafio era segurança, então listo alguns pontos forte do projeto:

* Validação ClientSide e ServerSide (~~Eu ia colocar um gerador de log de eventos, mas achei que era desnecessário agora, ~~) .... o log seria assim mais ou menos:



 *function logMsg ('$msg', $level = 'info', $file = 'olx.log')*

````

    $levelStr = '';
 
    switch ( $level )
    {
        case "info":
            $levelStr = 'INFO';
            break;
 
        case 'warning':
            $levelStr = 'WARNING';
            break;
 
        case 'error':
            $levelStr = 'ERROR';
            break;
    }
 
    $date = date( 'Y-m-d H:i:s' );
 
    $msg = sprintf( "[%s] [%s]: %s%s", $date, $levelStr, $msg, PHP_EOL );
 
    // escreve o log no arquivo

    file_put_contents( $file, $msg, FILE_APPEND );

````

e a chamada seria algo assim:

````logMsg( "Email xxx@xxx.com já cadastrado na base!",'error');````

Isso vai gerar a seguinte linha no arquivo de log

`[2017-16-02 23:05:13] [ERROR]: Email xxx@xxx.com já cadastrado na base!`

Enfim....apenas fica a solução proposta de log :)

* Outro ponto bacana foi usar bind , pois traz proteção à SQL Injection ao rodar queries.

* Vale lembrar que estou criptografando a senha do usuário, fiquei na dúvida em usar md5 , md5+salt ou **crypt** , Escolhi o crypt pois o md5 possui problemas de [ colisão](https://en.wikipedia.org/wiki/MD5#Collision_vulnerabilities)


###  O que foi utilizado para fazer o projeto? ###

* PHP
* HTML5 + CSS3 (utilizando flexbox para organizar a disposição dos elementos...e utilizando tags de HTML5 para validação no front)
* MYSQL


### O que este projeto faz? ###

* Cadastro de um formulário 
* Autenticação via Facebook para preencher campos específicos (email e nome) - **opcional** 
* Validação de força de senha com expressão regular ~~com uma tentativa de layout~~ que classifica a sua senha em fraca, média e forte 
* Validações Client side e Server side
* Validação de máscara de telefone para Portugal
* Validação de campo para NIF
* Autocomplete de localidade, baseado no código postal - *restrito a Potugal*
* Após o insert no form lista todos os usuários já cadastrados ( como apelido não é obrigatório, pode ser que sua coluna fique em branco, se o usuário não a preencher), isso foi um *plus*, apenas para ver os dados na tela.

### Como configuro o projeto? ###

Basta seguir esses passos (*mais rápido que possa imaginar!*):

**ATENÇÃO:**

*É necessário ter o [PHP + APACHE (ou nginx) + MYSQL](http://carlossouza.com/wp/tutorial_servidor_local) (segue um tutorial que fiz em umblog antigo meu, que vai ajudar tanto em linux, ou windows)*

*Outro ponto, é altamente recomendado que o projeto fique no diretório onde o seu servidor web está (ex: var/www/html no linux , ou mo htdocs no windows)*

*AGORA VAMOS LÁ:*

* 1 Acesse www.SEUHOST/olx (*simples assim*) , ele vai cair nessa página: ![Alt text](http://carlossouza.com/olx/img/bd1.png)

* 2 Nessa página voce deve configurar o ** HOST, DATABASE, USER** e ** SENHA** (*Para Crirar as tabelas, obrigatóriamente você deve validar a sua conexão...se estiver tudo ok, o sistema permite criar as tabelas)*

* 3 Uma vez que você configurou, o programa já deleta todos os configs que usamos para montar a estrutura..... E já te leva para o formulário, pronto para uso!

* 4 Caso você queira que o seu formulário de cadastro tenha integração com o facebook, para preencher automático nome e email ,deve crirar um [ app id no facebook ](https://developers.facebook.com/docs/apps/register)

* 5 Supondo que você decidiu se integrar com o Facebook e o Google, você deve alterar 2 trechos de códigos para a integração ocorrer de forma correta: (**OK...Deixei minhas keys, mas já aviso que a do Facebook não vai funcionar, pois o callback está configurado para o meu servidor apenas**)

**Facebook:**
* Na view form, linha 32:
````appId: '**YOUR APP ID ON FACEBOOK**',````

* 6 Parabéns! Você já pode usar 100% do projeto agora.

** O projeto foi feito na versão do php 7.0 (*mas testado na versão 5.6 e 7.1 também)* e Mysql 5.5 (testado na versão 5.6)**

### Quer falar comigo? ###

* [Telegram](https://web.telegram.org/#/im?p=@carlossouzadev)
* [Twitter](https://twitter.com/carlossouzadev)
