<?php

include 'model/User.php';
include 'model/DaoUser.php';
include 'config/Template.php';



try {
    $user = new User();

    $result = validadeRequired($_POST);

    if (count($result) > 0) {
        $tmpl = new Template('view/form.php', array("erros" => $result));
        echo $tmpl->render();
        exit;
    } else {

        $user->setAlias(filter_input(INPUT_POST, 'alias'));
        $user->setContry(filter_input(INPUT_POST, 'contry_id'));
        $user->setEmail(filter_input(INPUT_POST, 'email'));
        $user->setLocation(filter_input(INPUT_POST, 'location'));
        $user->setName(filter_input(INPUT_POST, 'name'));
        $user->setNif(filter_input(INPUT_POST, 'nif'));
        $user->setPassword(md5(filter_input(INPUT_POST, 'password')));
        $user->setPhone(filter_input(INPUT_POST, 'phone'));
        $user->setPostal_code(filter_input(INPUT_POST, 'postal_code'));
        $user->setStreet(filter_input(INPUT_POST, 'street'));

        DaoUser::Insert($user);
        $users = DaoUser::ListAll();

        $tmpl = new Template('view/list.php', array("result" => $users));
        echo $tmpl->render();
        exit;
    }
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}

function validadeRequired($params) {
    $errors = array();
    $required = array('email', 'name', 'password');

    if (count($params) == 0)
        $errors[] = 'Preencha o formulário.';

    if ($params['password'] !== $params['password2'])
        $errors[] = 'Erro na validação de senha';

    if ($params['email'] != $params['email2'])
        $errors[] = 'Erro na validação de e-mail';

    $email = DaoUser::SearchByEmail($params['email']);
    if (count($email) > 0) {
        $errors[] = 'Email já cadastrado na base.';
    }

    if (isset($array) && is_array($array)) {

        foreach ($array as $key => $value) {

            if (in_array($key, $required)) {
                if (empty($value))
                    $errors[] = 'Field: ' . $key . ' empty.';
            }
        }
    }

    return $errors;
}
