<?php

include 'Const.php';

class Database {

    public static $instance;
    public static $host = HOST;
    public static $dbname = DATABASE;
    public static $user = USER;
    public static $password = PASSWORD;

    private function __construct() {
        //
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new PDO('mysql:host=' . self::$host . ';dbname=' . self::$dbname . '', '' . self::$user . '', '' . self::$password . '', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$instance->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
        }

        return self::$instance;
    }

}

