<?php

if (filter_input(INPUT_POST, 'type') == 'valid') {
    try {
        $conn = new PDO("mysql:host=" . filter_input(INPUT_POST, 'host') . ";"
                . "dbname=" . filter_input(INPUT_POST, 'database') . "", filter_input(INPUT_POST, 'user'), filter_input(INPUT_POST, 'password'));

        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo json_encode(array('status' => '1'));
    } catch (PDOException $e) {
        echo json_encode(array('status' => '0', 'message' => $e->getMessage()));
    }
}

if (filter_input(INPUT_POST, 'type') == 'create') {
    try {
        $file = 'config/Const.php';
        $file_pointer = fopen($file, 'w');

        $string = '<?php'
                . ' define("HOST", "' . filter_input(INPUT_POST, 'host') . '");'
                . ' define("DATABASE", "' . filter_input(INPUT_POST, 'database') . '");'
                . ' define("USER", "' . filter_input(INPUT_POST, 'user') . '");'
                . ' define("PASSWORD", "' . filter_input(INPUT_POST, 'password') . '");'
                . ' ?>';

        fwrite($file_pointer, $string);
        fclose($file_pointer);

        include($file);
        
        $conn = new PDO("mysql:host=" . filter_input(INPUT_POST, 'host') . ";"
                . "dbname=" . filter_input(INPUT_POST, 'database') . "", filter_input(INPUT_POST, 'user'), filter_input(INPUT_POST, 'password'));

        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = file_get_contents('config/database.sql');
        $conn->query($sql);
        unlink('config/database.sql');

        echo json_encode(array('status' => '1'));
    } catch (PDOException $e) {
        echo json_encode(array('status' => '0', 'message' => $e->getMessage()));
    }
}